<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Company>
 */
class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "company" => "Dwi A Trans",
            "logo" => null,
            "about" => "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur, ab tenetur veritatis perspiciatis doloremque vero laboriosam quas facilis assumenda rem.",
            "born" => now(),
            "street" => fake()->streetAddress(),
            "city" => fake()->city(),
            "province" => "Jawa Timur",
            "country" => "Indonesia",
            "postal_code" => fake()->postcode(),
        ];
    }
}
