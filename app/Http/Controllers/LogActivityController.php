<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLog_activityRequest;
use App\Http\Requests\UpdateLog_activityRequest;
use App\Models\Log_activity;

class LogActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLog_activityRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Log_activity $log_activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Log_activity $log_activity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLog_activityRequest $request, Log_activity $log_activity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Log_activity $log_activity)
    {
        //
    }
}
