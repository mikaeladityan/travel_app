<?php

namespace App\Http\Controllers;

use App\Models\Log_activity;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
    public function register()
    {
        return view("auth.register");
    }

    public function login()
    {
        return view("auth.login");
    }

    // Create Register user
    public function store(Request $request)
    {
        // Check password confirmation is same or not
        if ($request->input("password") != $request->input("passConfirm")) {
            return back()->with('error', "Password dan Konfirmasi Password tidak sama!");
        }
        // Validate the request
        $data = $request->validate([
            "first_name" => 'required|min:2',
            "last_name" => 'required|min:2',
            "username" => 'required|min:2|unique:users,username',
            "email" => 'required|min:2|email:dns|unique:users,email',
            "password" => 'required|min:8|max:255'
        ]);

        $data['password'] = bcrypt($data['password']);

        User::create($data);

        // Log Activities
        Log_activity::create([
            'users' => $data['username'],
            "ip_address" => $request->ip(),
            "url"  => url()->current(),
            "status" => "success",
            "message" => "Melakukan pendaftaran member"
        ]);
        return redirect("login")->with("success", "Pendaftaran member berhasil");
    }

    // Process to authentication login
    public function authenticated(Request $request)
    {
        $login = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($login)) {
            $request->session()->regenerate();
            $user = Auth::User();

            // Log Activities
            Log_activity::create([
                'users' => $user->username,
                "ip_address" => $request->ip(),
                "url"  => url()->current(),
                "status" => "success",
                "message" => "Melakukan Login User"
            ]);
            if ($user->role == "member") {
                return redirect()->intended('/member/dashboard');
            } elseif ($user->role == "driver") {
                return redirect()->intended('/driver/dashboard');
            } else {
                return redirect()->intended('/panel/dashboard');
            }
        }


        return back()->with('error', 'Login Gagal!');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
}
