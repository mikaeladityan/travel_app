<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function edit(Request $request, $id)
    {
        return view('panel.company.edit', [
            'company' => Company::where('id', $id)->first(),
        ]);
    }
}
