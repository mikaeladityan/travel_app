<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('panel.dashboard', [
            "company" => Company::where('id', '9b83c86b-6930-477f-964d-891422eeffed')->first(),
        ]);
    }
}
