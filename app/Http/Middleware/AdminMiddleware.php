<?php

namespace App\Http\Middleware;

use App\Models\Log_activity;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (Auth::check() && Auth::user()->role == 'developer' || Auth::user()->role == 'owner' || Auth::user()->role == 'manager' || Auth::user()->role == 'staff') {
            return $next($request);
        }
        // Log Activities
        Log_activity::create([
            'users' => Auth::user()->username,
            "ip_address" => $request->ip(),
            "url"  => url()->current(),
            "status" => "warning",
            "message" => "Akses " + url()->current()
        ]);
        return redirect('/member/dashboard')->with("warning", "Akses terlarang!");
    }
}
