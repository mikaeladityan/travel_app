@if (Request::is('register') || Request::is('login'))
    <nav>
        <h1 class="py-3 text-xs text-center text-slate-500">Website by Laborare Indonesia</h1>
        <div class="w-11/12 mx-auto mt-5 text-center">
            @if (session()->has('success'))
                <div class="w-full px-5 py-2 bg-teal-300 rounded-lg">
                    <p class="text-sm font-medium text-center text-teal-600">{{ session('success') }}</p>
                </div>
            @endif
            @if (session()->has('error'))
                <div class="w-full px-5 py-2 bg-red-300 rounded-lg">
                    <p class="text-sm font-medium text-center text-red-600">{{ session('error') }}</p>
                </div>
            @endif
        </div>
    </nav>
@elseif (Request::is('panel*') || Request::is('member*') || Request::is('driver*'))
    <nav class="bg-slate-900 border-slate-600">
        <div class="flex flex-wrap items-center justify-between max-w-screen-xl p-4 mx-auto">
            <a href="{{ url()->current() }}" class="flex items-center space-x-3 rtl:space-x-reverse">
                <img src="{{ $company->logo == null ? 'https://www.dwiatrans.com/img/website/logo.svg' : asset('storage/' . $company->logo) }}"
                    class="h-12" alt="{{ $company->company }} - Logo" />
                <span
                    class="self-center hidden font-bold text-slate-200 whitespace-nowrap md:block">{{ $company->company }}</span>
            </a>
            <div class="flex items-center space-x-3 md:order-2 md:space-x-0 rtl:space-x-reverse">
                <button type="button" class="flex text-sm rounded-full bg-slate-800 md:me-0 focus:ring-4"
                    id="user-menu-button" aria-expanded="false" data-dropdown-toggle="user-dropdown"
                    data-dropdown-placement="bottom">
                    <span class="self-center px-3 text-xs font-semibold text-slate-200">Hi,
                        {{ Auth::user()->first_name }}</span>
                    <img class="w-8 h-8 rounded-full"
                        src="{{ Auth::user()->photo == null ? asset('storage/images/default_user.png') : asset('storage/' . Auth::user()->photo) }}"
                        alt="user photo">
                </button>
                <!-- Dropdown menu -->
                <div class="z-50 hidden my-4 text-base list-none divide-y rounded-lg shadow bg-slate-700 divide-slate-200"
                    id="user-dropdown">
                    <div class="px-4 py-3">
                        <span
                            class="block text-sm text-slate-200">{{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</span>
                        <span class="block text-sm truncate text-slate-400">{{ Auth::user()->email }}</span>
                    </div>
                    <ul class="py-2" aria-labelledby="user-menu-button">
                        <li>
                            <a href="#"
                                class="block px-4 py-2 text-sm hover:bg-slate-800 text-slate-200 hover:text-slate-300">Pengaturan</a>
                        </li>
                        <li>
                            <a href="#"
                                class="block px-4 py-2 text-sm hover:bg-slate-800 text-slate-200 hover:text-slate-300">Pesanan</a>
                        </li>
                        <li>
                            <a href="#"
                                class="block px-4 py-2 text-sm hover:bg-slate-800 text-slate-200 hover:text-slate-300">Pemberitahuan</a>
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button type="submit"
                                    class="block w-full px-4 py-2 text-sm text-start hover:bg-slate-800 text-slate-200 hover:text-slate-300">Keluar</button>
                            </form>
                        </li>
                    </ul>
                </div>
                <button data-collapse-toggle="navbar-user" type="button"
                    class="inline-flex items-center justify-center w-10 h-10 p-2 text-sm rounded-lg text-slate-500 md:hidden hover:bg-slate-800 focus:outline-none focus:ring-2 focus:ring-slate-200"
                    aria-controls="navbar-user" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                        viewBox="0 0 17 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M1 1h15M1 7h15M1 13h15" />
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-user">
                <ul
                    class="flex flex-col p-4 mt-4 font-medium border rounded-lg border-slate-100 md:p-0 bg-slate-900 md:space-x-8 rtl:space-x-reverse md:flex-row md:mt-0 md:border-0 md:bg-slate-900">
                    <li>
                        <a href="#" class="block px-3 py-2 rounded text-slate-200 md:bg-transparent md:p-0"
                            aria-current="page">Dashboard</a>
                    </li>
                    @if (Auth::user()->role == 'developer' || Auth::user()->role == 'owner')
                        <li>
                            <a href="{{ url('/panel/company/9b83c86b-6930-477f-964d-891422eeffed') }}"
                                class="block px-3 py-2 rounded text-slate-200 hover:bg-slate-800 md:hover:bg-transparent md:p-0 hover:text-slate-300 border-slate-800">Perusahaan</a>
                        </li>
                    @endif
                    <li>
                        <a href="#"
                            class="block px-3 py-2 rounded text-slate-200 hover:bg-slate-800 md:hover:bg-transparent md:p-0 hover:text-slate-300 border-slate-800">Pesanan</a>
                    </li>
                    <li>
                        <a href="#"
                            class="block px-3 py-2 rounded text-slate-200 hover:bg-slate-800 md:hover:bg-transparent md:p-0 hover:text-slate-300 border-slate-800">Layanan</a>
                    </li>
                    <li>
                        <a href="#"
                            class="block px-3 py-2 rounded text-slate-200 hover:bg-slate-800 md:hover:bg-transparent md:p-0 hover:text-slate-300 border-slate-800">Driver</a>
                    </li>
                    <li>
                        <a href="#"
                            class="block px-3 py-2 rounded text-slate-200 hover:bg-slate-800 md:hover:bg-transparent md:p-0 hover:text-slate-300 border-slate-800">Member</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
@else
@endif
