@extends('layouts.main')
@section('body')
    <section class="flex items-center justify-center w-full min-h-screen px-3">
        <div>
            <div class="text-center">
                <h1 class="font-bold text-3xl text-slate-800">Masuk</h1>
                <p class="text-slate-600 mt-3">Lorem ipsum dolor sit, amet consectetur
                    adipisicing
                    elit.
                    Incidunt, harum.</p>
            </div>

            {{-- Form Register --}}
            <div class="mt-5 px-8">
                <form action="{{ route('login') }}" method="post">
                    @csrf

                    {{-- Username --}}
                    <div class="w-full">
                        <label for="username" class="text-sm text-slate-700 font-medium">Username</label>
                        <input type="text" class="form-input" name="username" autocomplete="off" autofocus>
                    </div>
                    {{-- Password --}}
                    <div class="w-full mt-2">
                        <label for="password" class="text-sm text-slate-700 font-medium">Password</label>
                        <input type="password" class="form-input" name="password">
                    </div>

                    {{-- Button --}}
                    <button
                        class="text-sm uppercase font-medium w-full flex items-center justify-center py-3 text-gray-200 bg-slate-800 mt-5 rounded-lg shadow-lg">
                        Masuk
                    </button>

                    <p class="mt-5 text-slate-500 text-sm font-medium text-center">
                        Apakah belum punya member? <a href="{{ route('page.register') }}" class="underline">Daftar</a>
                    </p>
                </form>
            </div>
        </div>
    </section>
@endsection
