@extends('layouts.main')
@section('body')
    <section class="flex items-center justify-center w-full min-h-screen px-3">
        <div>
            <div class="text-center">
                <h1 class="font-bold text-3xl text-slate-800">Daftar</h1>
                <p class="text-slate-600 mt-3">Lorem ipsum dolor sit, amet consectetur
                    adipisicing
                    elit.
                    Incidunt, harum.</p>
            </div>

            {{-- Form Register --}}
            <div class="mt-5 px-8">
                <form action="{{ route('registration') }}" method="post">
                    @csrf

                    <div class="flex items-center justify-center gap-2">
                        {{-- First Name --}}
                        <div class="w-full">
                            <label for="first_name" class="text-sm text-slate-700 font-medium">Nama Depan</label>
                            <input type="text" class="form-input" name="first_name" autocomplete="off"
                                value="{{ old('first_name') }}" autofocus required>
                        </div>

                        {{-- First Name --}}
                        <div class="w-full">
                            <label for="last_name" class="text-sm text-slate-700 font-medium">Nama Belakang</label>
                            <input type="text" class="form-input" name="last_name" autocomplete="off"
                                value="{{ old('last_name') }}" required>
                        </div>
                    </div>

                    {{-- Username --}}
                    <div class="w-full mt-2">
                        <label for="username" class="text-sm text-slate-700 font-medium">Username</label>
                        <input type="text" class="form-input" name="username" autocomplete="off"
                            value="{{ old('username') }}" required>
                    </div>

                    {{-- Email --}}
                    <div class="w-full mt-2">
                        <label for="email" class="text-sm text-slate-700 font-medium">Email</label>
                        <input type="email" class="form-input" name="email" autocomplete="off"
                            value="{{ old('email') }}" required>
                    </div>

                    <div class="flex items-center justify-center gap-2 mt-2">
                        {{-- Password --}}
                        <div class="w-full">
                            <label for="password" class="text-sm text-slate-700 font-medium">Password</label>
                            <input type="password" class="form-input" name="password">
                        </div>

                        {{-- Password Confirmation --}}
                        <div class="w-full">
                            <label for="passConfirm" class="text-sm text-slate-700 font-medium">Konfirmasi</label>
                            <input type="password" class="form-input" name="passConfirm">
                        </div>
                    </div>

                    {{-- Button --}}
                    <button
                        class="text-sm uppercase font-medium w-full flex items-center justify-center py-3 text-gray-200 bg-slate-800 mt-5 rounded-lg shadow-lg">
                        Daftar Sekarang
                    </button>

                    <p class="mt-5 text-slate-500 text-sm font-medium text-center">
                        Apakah sudah menjadi member? <a href="{{ route('page.login') }}" class="underline">Masuk</a>
                    </p>
                </form>
            </div>
        </div>
    </section>
@endsection
