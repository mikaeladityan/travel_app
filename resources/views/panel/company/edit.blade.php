@extends('layouts.main')
@section('body')
    <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="flex items-start justify-center gap-5">

            <div class="w-9/12">
                <div class="card-dark">
                    <div class="flex items-center justify-start gap-4">
                        <div class="w-6/12">
                            <label class="text-sm font-semibold">Kode Perusahaan</label>
                            <input type="text" class="input" value="{{ $company->id }}" disabled>
                        </div>
                    </div>
                    <div class="flex items-center justify-start w-full gap-4 mt-4">
                        <div class="w-3/12">
                            <label for="company" class="text-sm font-semibold">Nama Perusahaan</label>
                            <input type="text"
                                class="input @if ($errors->has('company')) border-red-600 text-red-600 @endif"
                                value="{{ old('company', $company->company) }}" name="company">
                        </div>
                        <div class="w-2/12">
                            <label for="born" class="text-sm font-semibold">Berdiri Sejak</label>
                            <input type="date"
                                class="input @if ($errors->has('born')) border-red-600 text-red-600 @endif"
                                value="{{ old('born', $company->born) }}" name="born" style="color-scheme: dark;">
                        </div>
                    </div>
                    <div class="flex items-center justify-start mt-4">
                        <div class="w-6/12">
                            <label for="about" class="text-sm font-semibold">Tentang Perusahaan</label>
                            <textarea name="about" class="input @if ($errors->has('company')) border-red-600 text-red-600 @endif"
                                rows="3">{{ old('company', $company->about) }}</textarea>
                        </div>
                    </div>
                </div>

                {{-- Social Media --}}
                <div class="mt-8 card-dark">
                    <div class="flex items-center justify-between">
                        <h5 class="text-xs font-semibold uppercase text-slate-400">Sosial Media {{ $company->company }}</h5>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"
                            style="transform: ;msFilter:;" class="fill-slate-400">
                            <path
                                d="M3 12c0 1.654 1.346 3 3 3 .794 0 1.512-.315 2.049-.82l5.991 3.424c-.018.13-.04.26-.04.396 0 1.654 1.346 3 3 3s3-1.346 3-3-1.346-3-3-3c-.794 0-1.512.315-2.049.82L8.96 12.397c.018-.131.04-.261.04-.397s-.022-.266-.04-.397l5.991-3.423c.537.505 1.255.82 2.049.82 1.654 0 3-1.346 3-3s-1.346-3-3-3-3 1.346-3 3c0 .136.022.266.04.397L8.049 9.82A2.982 2.982 0 0 0 6 9c-1.654 0-3 1.346-3 3z">
                            </path>
                        </svg>
                    </div>
                </div>
            </div>



            <div class="w-3/12">
                <div class="flex items-center justify-center w-full shadow-none card-dark min-h-[9.5rem]">
                    <a href="/"
                        class="w-8/12 px-3 py-2 mx-auto text-sm font-semibold text-center uppercase bg-teal-600 rounded-lg">[+]
                        Bank</a>
                </div>
            </div>
        </div>

    </form>
@endsection
