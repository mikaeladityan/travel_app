@extends('layouts.main')
@section('body')
    <form action="{{ route('logout') }}" method="POST">
        @csrf
        <button type="submit" class="bg-red-600">Log Out</button>
    </form>
@endsection
