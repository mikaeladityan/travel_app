<?php

use App\Http\Controllers\admin\CompanyController;
use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\member\DashboardController as MemberDashboardController;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\MemberMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route for authentication
Route::get('/register', [AuthenticationController::class, 'register'])->name("page.register");
Route::post('/register', [AuthenticationController::class, 'store'])->name("registration");
Route::get('/login', [AuthenticationController::class, 'login'])->name("page.login");
Route::post('/authenticated', [AuthenticationController::class, 'authenticated'])->name("login");
Route::post('/logout', [AuthenticationController::class, 'logout'])->name("logout");

// Dashboard ADMIN
Route::prefix("panel")->middleware('auth', AdminMiddleware::class)->group(function () {
    Route::get("/dashboard", [AdminDashboardController::class, 'index'])->name("dashboard");
    // Update Companies
    Route::get("/company/{id}", [CompanyController::class, 'edit'])->name("company.edit");
});

// Dashboard Member
Route::prefix("member")->middleware('auth', MemberMiddleware::class)->group(function () {
    Route::get("/dashboard", [MemberDashboardController::class, 'index'])->name("dashboard");
});

// Dashboard Driver
